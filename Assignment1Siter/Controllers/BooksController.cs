﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Assignment1Siter;
using Assignment1Siter.Models;
using AutoMapper;
using Assignment1Siter.DataTransferObjects.BookDTO;

namespace Assignment1Siter.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    [Consumes("application/json")]
    public class BooksController : ControllerBase
    {
        #region Constructor
        private readonly RentabookDBcontext _context;
        private readonly IMapper _mapper;

        public BooksController(RentabookDBcontext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        #endregion
        /// <summary>
        /// Get all the books from Book
        /// </summary>
        /// <returns>List of Book DTOs to the client</returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]


        // GET: api/Books
        [HttpGet]
        public async Task<ActionResult<IEnumerable<DTOReadBook>>> GetBooks()
        {
          if (_context.Books == null)
          {
              return NotFound();
          }
            var bookModel =  await _context.Books.ToListAsync();

            var readerDto = _mapper.Map<List<DTOReadBook>>(bookModel);
            return readerDto;
        }
        /// <summary>
        /// Get a single book by searching the Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Return only a single book</returns>
        // GET: api/Books/5
        [HttpGet("{id}")]
        public async Task<ActionResult<DTOReadBook>> GetBook(int id)
        {
          if (_context.Books == null)
          {
              return NotFound();
          }
            var book = await _context.Books.FindAsync(id);

            if (book == null)
            {
                return NotFound();
            }

            return _mapper.Map<DTOReadBook>(book);
        }
        /// <summary>
        /// Updates a book in Book for a required Id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="book"></param>
        /// <returns>Updates the required Id with a new Value</returns>
        // PUT: api/Books/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutBook(int id, Book book)
        {
            if (id != book.BookId)
            {
                return BadRequest();
            }

            _context.Entry(book).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BookExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }
        /// <summary>
        /// Post a book to Book
        /// </summary>
        /// <param name="bookDto"></param>
        /// <returns></returns>
        // POST: api/Books
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<DTOReadBook>> PostBook(DTOCreateBook bookDto)
        {
          if (_context.Books == null)
          {
              return Problem("Entity set 'RentabookDBcontext.Books'  is null.");
          }
          var bookModel = _mapper.Map<Book>(bookDto);

            _context.Books.Add(bookModel);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetBook", new { id = bookModel.BookId }, bookDto);
        }
        /// <summary>
        /// Deletes a book from Book for a specific Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Deletes a specific book with a requested Id</returns>
        // DELETE: api/Books/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteBook(int id)
        {
            if (_context.Books == null)
            {
                return NotFound();
            }
            var book = await _context.Books.FindAsync(id);
            if (book == null)
            {
                return NotFound();
            }

            _context.Books.Remove(book);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool BookExists(int id)
        {
            return (_context.Books?.Any(e => e.BookId == id)).GetValueOrDefault();
        }
    }
}
