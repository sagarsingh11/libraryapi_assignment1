﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Assignment1Siter.Models
{
    [Table(nameof(Book))]
    public class Book
    {
        public int BookId { get; set; }
        [StringLength(50)]
        public string Title { get; set; }
        public string Author { get; set; }

        // Navigation
        public ICollection<Reader> Readers { get; set; } //M-M        
        public int? LibraryId { get; set; } /// 1-1 /
        public Library Library { get; set; } //1-M
    }
}
