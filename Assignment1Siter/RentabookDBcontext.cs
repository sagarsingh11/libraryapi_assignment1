﻿using Assignment1Siter.Models;
using Microsoft.EntityFrameworkCore;
using System.Data;

namespace Assignment1Siter
{
    public class RentabookDBcontext:DbContext
    {
        public RentabookDBcontext(DbContextOptions options) : base(options)
        {

        }
        public DbSet<Book> Books { get; set; }
        public DbSet<Reader> Readers { get; set; }
        public DbSet<Library> Libraries { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();

            var connectionString = configuration.GetConnectionString("DefaultConnection");
            optionsBuilder.UseSqlServer(connectionString);
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // TODO: add data from SeedData
            modelBuilder.Entity<Library>().HasData(Dataseed.GetLibraries());
            modelBuilder.Entity<Book>().HasData(Dataseed.GetBooks());
            modelBuilder.Entity<Reader>().HasData(Dataseed.GetReaders());

            modelBuilder.Entity<Reader>()
                .HasMany(m => m.Books)
                .WithMany(c => c.Readers)
                .UsingEntity<Dictionary<string, object>>(
                "Bookreader",
                r => r.HasOne<Book>().WithMany().HasForeignKey("BookId"),
                l => l.HasOne<Reader>().WithMany().HasForeignKey("ReaderId"),
                je =>
                {
                    je.HasKey("BookId", "ReaderId");
                    je.HasData(
                    new { BookId = 1, ReaderId = 3 },
                    new { BookId = 2, ReaderId = 1 },
                    new { BookId = 3, ReaderId = 2 },
                    new { BookId = 1, ReaderId = 1 }
                    );
                });
        }
    }
}
