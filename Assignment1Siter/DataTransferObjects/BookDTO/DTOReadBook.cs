﻿using Assignment1Siter.Models;
using System.ComponentModel.DataAnnotations;

namespace Assignment1Siter.DataTransferObjects.BookDTO
{
    public class DTOReadBook
    {

        [StringLength(50)]
        public string Title { get; set; }
        public string Author { get; set; }

       
    }
}
