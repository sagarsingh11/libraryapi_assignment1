﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Assignment1Siter.Models
{
    [Table(nameof(Reader))]
    public class Reader
    {

        public int ReaderId { get; set; }
        [StringLength(50)]
        public string Name { get; set; }
        public int Age { get; set; }
        [StringLength(150)]
        public string Email { get; set; }

        // Navigation

        public ICollection<Book> Books { get; set; } = null!;// M-1
    }
}
