﻿using Assignment1Siter.Models;
using System.ComponentModel.DataAnnotations;

namespace Assignment1Siter.DataTransferObjects.ReaderDTO
{
    //this class is for DTO 
    public class DTOCreateReader
    {

        
        [StringLength(50)]
        public string Name { get; set; }
        
        [StringLength(150)]
        public string Email { get; set; }

       
    }
}
