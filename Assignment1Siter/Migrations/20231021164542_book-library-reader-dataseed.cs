﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Assignment1Siter.Migrations
{
    public partial class booklibraryreaderdataseed : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Library",
                columns: table => new
                {
                    LibraryId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    City = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Library", x => x.LibraryId);
                });

            migrationBuilder.CreateTable(
                name: "Reader",
                columns: table => new
                {
                    ReaderId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    Age = table.Column<int>(type: "int", nullable: false),
                    Email = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Reader", x => x.ReaderId);
                });

            migrationBuilder.CreateTable(
                name: "Book",
                columns: table => new
                {
                    BookId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    Author = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    LibraryId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Book", x => x.BookId);
                    table.ForeignKey(
                        name: "FK_Book_Library_LibraryId",
                        column: x => x.LibraryId,
                        principalTable: "Library",
                        principalColumn: "LibraryId");
                });

            migrationBuilder.CreateTable(
                name: "Bookreader",
                columns: table => new
                {
                    BookId = table.Column<int>(type: "int", nullable: false),
                    ReaderId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Bookreader", x => new { x.BookId, x.ReaderId });
                    table.ForeignKey(
                        name: "FK_Bookreader_Book_BookId",
                        column: x => x.BookId,
                        principalTable: "Book",
                        principalColumn: "BookId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Bookreader_Reader_ReaderId",
                        column: x => x.ReaderId,
                        principalTable: "Reader",
                        principalColumn: "ReaderId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Library",
                columns: new[] { "LibraryId", "City", "Name" },
                values: new object[,]
                {
                    { 1, "Delhi", "Central Library" },
                    { 2, "Muscat", "City public library" }
                });

            migrationBuilder.InsertData(
                table: "Reader",
                columns: new[] { "ReaderId", "Age", "Email", "Name" },
                values: new object[,]
                {
                    { 1, 34, "wilhader@gmail.com", "William hader" },
                    { 2, 37, "Ptrwill@gmail.com", "peter williams" },
                    { 3, 29, "Benehunt@gmail.com", "Benedict hunter" }
                });

            migrationBuilder.InsertData(
                table: "Book",
                columns: new[] { "BookId", "Author", "LibraryId", "Title" },
                values: new object[] { 1, "Animation,Adventure", 1, "inside Out 1" });

            migrationBuilder.InsertData(
                table: "Book",
                columns: new[] { "BookId", "Author", "LibraryId", "Title" },
                values: new object[] { 2, "William shakerspere", 1, "As you like it" });

            migrationBuilder.InsertData(
                table: "Book",
                columns: new[] { "BookId", "Author", "LibraryId", "Title" },
                values: new object[] { 3, "Sidney Sheldon", 1, "if tomorwow comes" });

            migrationBuilder.InsertData(
                table: "Bookreader",
                columns: new[] { "BookId", "ReaderId" },
                values: new object[,]
                {
                    { 1, 1 },
                    { 1, 3 },
                    { 2, 1 },
                    { 3, 2 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Book_LibraryId",
                table: "Book",
                column: "LibraryId");

            migrationBuilder.CreateIndex(
                name: "IX_Bookreader_ReaderId",
                table: "Bookreader",
                column: "ReaderId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Bookreader");

            migrationBuilder.DropTable(
                name: "Book");

            migrationBuilder.DropTable(
                name: "Reader");

            migrationBuilder.DropTable(
                name: "Library");
        }
    }
}
