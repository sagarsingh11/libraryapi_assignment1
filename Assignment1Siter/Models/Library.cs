﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Assignment1Siter.Models
{
    [Table(nameof(Library))]
    public class Library
    {

        public int LibraryId { get; set; }
        [StringLength(100)]
        public string Name { get; set; }
        [StringLength(100)]
        public string City { get; set; }

        // Navigation         
        public ICollection<Book> Books { get; set; }  // M-1
    }
}
