﻿using Assignment1Siter.Models;

namespace Assignment1Siter
{
    public class Dataseed
    {
        public static List<Book> GetBooks()
        {
            List<Book> books = new()
            {
                new Book()
                {
                    BookId = 1,
                    Title = "inside Out 1",
                    Author = "Animation,Adventure",
                    LibraryId = 1
                },
                new Book()
                {
                    BookId = 2,
                    Title = "As you like it",
                    Author = "William shakerspere",
                    LibraryId = 1
                },
                new Book()
                {
                    BookId = 3,
                    Title = "if tomorwow comes",
                    Author = "Sidney Sheldon",
                    LibraryId = 1
                }
            };
            return books;
        }
        public static List<Reader> GetReaders()
        {
            List<Reader> readers = new()

            {
                new Reader()
                {
                    ReaderId = 1,
                    Name = "William hader",
                    Age = 34,
                    Email = "wilhader@gmail.com",
                },
                new Reader()
                {
                    ReaderId = 2,
                    Name = "peter williams",
                    Age = 37,
                    Email = "Ptrwill@gmail.com",
                },
                new Reader()
                {
                    ReaderId = 3,
                    Name = "Benedict hunter",
                    Age = 29,
                    Email = "Benehunt@gmail.com",
                }
            };
            return readers;
        }
        public static List<Library> GetLibraries()
        {
            List<Library> libraries = new()
                {
                new Library()
                {
                LibraryId = 1,
                Name = "Central Library",
                City = "Delhi"
                },
                new Library()
                {
                LibraryId = 2,
                Name = "City public library",
                City = "Muscat"
                }
                };
            return libraries;
        }
    }
}
