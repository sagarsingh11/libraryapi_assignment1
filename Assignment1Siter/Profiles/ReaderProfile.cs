﻿using Assignment1Siter.DataTransferObjects.ReaderDTO;
using Assignment1Siter.Models;
using AutoMapper;

namespace Assignment1Siter.Profiles
{
    public class ReaderProfile : Profile
    {
        public ReaderProfile()
        {
            CreateMap<Reader, DTOReadReader>();
            CreateMap<DTOCreateReader, Reader>();
        }
    }
}
