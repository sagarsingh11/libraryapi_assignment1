﻿using Assignment1Siter.DataTransferObjects.BookDTO;
using Assignment1Siter.DataTransferObjects.ReaderDTO;
using Assignment1Siter.Models;
using AutoMapper;

namespace Assignment1Siter.Profiles
{
    public class BookProfile: Profile
    {
        public BookProfile()
        {
            CreateMap<Book, DTOReadBook>();
            CreateMap<DTOCreateBook, Book>();
        }
    }
}
